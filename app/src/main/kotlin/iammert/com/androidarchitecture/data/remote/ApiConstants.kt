package iammert.com.androidarchitecture.data.remote

/**
 * Created by mertsimsek on 19/05/2017.
 */
object ApiConstants {
    const val ENDPOINT = "https://api.themoviedb.org/3/"
    const val IMAGE_ENDPOINT_PREFIX = "https://image.tmdb.org/t/p/w500/"

    //please use your own API-KEY here, go to https://www.themoviedb.org/documentation/api?language=en
    const val API_KEY = "78716d83195aed1f9c33db217b5f66d4"
    const val TIMEOUT_IN_SEC = 15
}