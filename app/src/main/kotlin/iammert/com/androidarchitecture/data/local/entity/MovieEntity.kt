package iammert.com.androidarchitecture.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Created by mertsimsek on 19/05/2017.
 */
@Entity(tableName = "movies")
class MovieEntity {
    @PrimaryKey
    @SerializedName("id")
    var id = 0

    @SerializedName("poster_path")
    var posterPath: String? = null

    @SerializedName("adult")
    var isAdult = false

    @SerializedName("overview")
    var overview: String? = null

    @SerializedName("original_title")
    var originalTitle: String? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("vote_count")
    var voteCount = 0

    @SerializedName("vote_average")
    var voteAverage = 0.0

    @SerializedName("backdrop_path")
    var backdropPath: String? = null

    @SerializedName("original_language")
    var originalLanguage: String? = null
}