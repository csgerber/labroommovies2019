package iammert.com.androidarchitecture

import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import iammert.com.androidarchitecture.di.DaggerAppComponent
import javax.inject.Inject

/**
 * Created by mertsimsek on 20/05/2017.
 */
class AAApp : Application(), HasAndroidInjector {
    @kotlin.jvm.JvmField
    @Inject
    var activityDispatchingInjector: DispatchingAndroidInjector<Any>? = null
    override fun onCreate() {
        super.onCreate()
        initializeComponent()
    }

    private fun initializeComponent() {
        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this)
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return activityDispatchingInjector!!
    }
}