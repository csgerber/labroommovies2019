package iammert.com.androidarchitecture.databinding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import iammert.com.androidarchitecture.data.Resource
import iammert.com.androidarchitecture.ui.BaseAdapter

/**
 * Created by mertsimsek on 20/05/2017.
 */
object ListBindingAdapter {
    @JvmStatic
    @BindingAdapter(value = ["resource"])
    fun setResource(recyclerView: RecyclerView, resource: Resource<*>) {
        val adapter = recyclerView.adapter ?: return
        if (resource.data == null) return


        if (adapter is BaseAdapter<*, *>) {
            adapter.setData(resource.data as List<Nothing>)
        }
    }
}