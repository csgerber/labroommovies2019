package iammert.com.androidarchitecture.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import iammert.com.androidarchitecture.data.local.entity.MovieEntity
import iammert.com.androidarchitecture.databinding.ItemMovieListBinding
import iammert.com.androidarchitecture.ui.BaseAdapter
import iammert.com.androidarchitecture.ui.main.MovieListAdapter.MovieViewHolder
import java.util.*

/**
 * Created by mertsimsek on 20/05/2017.
 */
class MovieListAdapter(movieListCallback: MovieListCallback) : BaseAdapter<MovieViewHolder, MovieEntity>() {
    private var movieEntities: List<MovieEntity>
    private val movieListCallback: MovieListCallback
    override fun setData(movieEntities: List<MovieEntity>) {
        this.movieEntities = movieEntities
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MovieViewHolder {
        return MovieViewHolder.create(LayoutInflater.from(viewGroup.context), viewGroup, movieListCallback)
    }

    override fun onBindViewHolder(viewHolder: MovieViewHolder, i: Int) {
        viewHolder.onBind(movieEntities[i])
    }

    override fun getItemCount(): Int {
        return movieEntities.size
    }

    class MovieViewHolder(var binding: ItemMovieListBinding, callback: MovieListCallback) : RecyclerView.ViewHolder(binding.root) {
        fun onBind(movieEntity: MovieEntity?) {
            binding.movie = movieEntity
            binding.executePendingBindings()
        }

        companion object {
            fun create(inflater: LayoutInflater?, parent: ViewGroup?, callback: MovieListCallback): MovieViewHolder {
                val itemMovieListBinding = ItemMovieListBinding.inflate(inflater!!, parent, false)
                return MovieViewHolder(itemMovieListBinding, callback)
            }
        }

        init {
            binding.root.setOnClickListener { v: View? ->
                binding.movie?.let {
                    callback.onMovieClicked(it, binding.imageViewCover)
                }
            }
        }
    }

    init {
        movieEntities = ArrayList()
        this.movieListCallback = movieListCallback
    }
}