package iammert.com.androidarchitecture.ui.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.DaggerAppCompatActivity
import iammert.com.androidarchitecture.R
import iammert.com.androidarchitecture.data.local.entity.MovieEntity
import iammert.com.androidarchitecture.databinding.ActivityMovieDetailBinding
import javax.inject.Inject

/**
 * Created by mertsimsek on 19/05/2017.
 */
class MovieDetailActivity : DaggerAppCompatActivity() {
    lateinit var binding: ActivityMovieDetailBinding

    @JvmField
    @Inject
    var viewModelFactory: ViewModelProvider.Factory? = null

    lateinit var movieDetailViewModel: MovieDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_movie_detail)
        movieDetailViewModel = ViewModelProviders.of(this, viewModelFactory).get(MovieDetailViewModel::class.java)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val movieId = intent.getIntExtra(KEY_MOVIE_ID, 0)
        movieDetailViewModel.getMovie(movieId).observe(this, { movieEntity: MovieEntity? -> binding.movie = movieEntity })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> ActivityCompat.finishAfterTransition(this)
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val KEY_MOVIE_ID = "key_movie_id"
        fun newIntent(context: Context?, movieId: Int): Intent {
            val intent = Intent(context, MovieDetailActivity::class.java)
            intent.putExtra(KEY_MOVIE_ID, movieId)
            return intent
        }
    }
}