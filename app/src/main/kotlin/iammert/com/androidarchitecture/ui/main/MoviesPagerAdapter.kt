package iammert.com.androidarchitecture.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

/**
 * Created by mertsimsek on 20/05/2017.
 */
class MoviesPagerAdapter(fm: FragmentManager?) : FragmentStatePagerAdapter(fm!!) {
    override fun getItem(i: Int): Fragment {
        return MovieListFragment.newInstance()
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }

    companion object {
        private val titles = arrayOf("Popular", "Science", "Comedy")
    }
}