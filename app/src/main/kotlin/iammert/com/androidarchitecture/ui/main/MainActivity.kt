package iammert.com.androidarchitecture.ui.main

import android.os.Bundle
import iammert.com.androidarchitecture.R
import iammert.com.androidarchitecture.databinding.ActivityMainBinding
import iammert.com.androidarchitecture.ui.BaseActivity

class MainActivity : BaseActivity<ActivityMainBinding>() {
    override val layoutRes: Int
        get() = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding.viewPager.adapter = MoviesPagerAdapter(supportFragmentManager)
        dataBinding.tabs.setupWithViewPager(dataBinding!!.viewPager)
        dataBinding.viewPager.offscreenPageLimit = 3
    }
}