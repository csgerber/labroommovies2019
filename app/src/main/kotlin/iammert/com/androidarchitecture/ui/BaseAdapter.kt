package iammert.com.androidarchitecture.ui

import androidx.recyclerview.widget.RecyclerView

/**
 * Created by mertsimsek on 21/05/2017.
 */
abstract class BaseAdapter<Type : RecyclerView.ViewHolder, Data> : RecyclerView.Adapter<Type>() {
    abstract fun setData(data: List<Data>)
}