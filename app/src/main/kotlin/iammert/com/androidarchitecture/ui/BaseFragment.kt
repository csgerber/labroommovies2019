package iammert.com.androidarchitecture.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * Created by mertsimsek on 15/09/2017.
 */
abstract class BaseFragment<VM : ViewModel, DB : ViewDataBinding> : Fragment(), LifecycleRegistryOwner {
    private val lifecycleRegistry = LifecycleRegistry(this)

    @JvmField
    @Inject
    var viewModelFactory: ViewModelProvider.Factory? = null

    lateinit var viewModel: VM
    lateinit var dataBinding: DB

    abstract fun getViewModel(): Class<VM>

    @get:LayoutRes
    abstract val layoutRes: Int
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(getViewModel())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBinding = DataBindingUtil.inflate(inflater, layoutRes, container, false)
        return dataBinding.root
    }

    override fun getLifecycle(): LifecycleRegistry {
        return lifecycleRegistry
    }
}