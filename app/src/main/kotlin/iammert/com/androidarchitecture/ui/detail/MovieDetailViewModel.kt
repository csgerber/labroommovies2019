package iammert.com.androidarchitecture.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import iammert.com.androidarchitecture.data.MovieRepository
import iammert.com.androidarchitecture.data.Resource
import iammert.com.androidarchitecture.data.local.entity.MovieEntity
import javax.inject.Inject

/**
 * Created by mertsimsek on 21/05/2017.
 */
class MovieDetailViewModel @Inject constructor(private val movieRepository: MovieRepository) : ViewModel() {
    private val movieDetail: LiveData<Resource<MovieEntity>> = MutableLiveData()

    fun getMovie(id: Int): LiveData<MovieEntity> {
        return movieRepository.getMovie(id)
    }
}