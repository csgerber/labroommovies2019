package iammert.com.androidarchitecture.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.recyclerview.widget.GridLayoutManager
import dagger.android.support.AndroidSupportInjection
import iammert.com.androidarchitecture.R
import iammert.com.androidarchitecture.data.Resource
import iammert.com.androidarchitecture.data.local.entity.MovieEntity
import iammert.com.androidarchitecture.databinding.FragmentMovieListBinding
import iammert.com.androidarchitecture.ui.BaseFragment
import iammert.com.androidarchitecture.ui.detail.MovieDetailActivity

/**
 * Created by mertsimsek on 19/05/2017.
 */
class MovieListFragment : BaseFragment<MovieListViewModel, FragmentMovieListBinding>(), MovieListCallback {
    override fun getViewModel(): Class<MovieListViewModel> {
        return MovieListViewModel::class.java
    }

    override val layoutRes: Int
        get() = R.layout.fragment_movie_list

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        dataBinding.recyclerView.layoutManager = GridLayoutManager(activity, 2)
        dataBinding.recyclerView.adapter = MovieListAdapter(this)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.popularMovies.observe(viewLifecycleOwner, { listResource: Resource<List<MovieEntity>> -> dataBinding.resource = listResource })
    }

    override fun onMovieClicked(movieEntity: MovieEntity, sharedView: View?) {
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity!!, sharedView!!, getString(R.string.shared_image))
        startActivity(MovieDetailActivity.newIntent(activity, movieEntity.id), options.toBundle())
    }

    companion object {
        fun newInstance(): MovieListFragment {
            val args = Bundle()
            val fragment = MovieListFragment()
            fragment.arguments = args
            return fragment
        }
    }
}